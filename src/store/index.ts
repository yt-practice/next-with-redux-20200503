import { combineReducers, createStore } from '@reduxjs/toolkit'
import { createAction, createSlice } from '@reduxjs/toolkit'
import { useSelector as origUseSelector, useDispatch } from 'react-redux'
import { useCallback } from 'react'

const tick = createAction('TICK', (light: boolean) => ({
	payload: {
		light: light,
		lastUpdate: Date.now(),
	},
}))

const increment = createAction('INCREMENT')
const decrement = createAction('DECREMENT')
const reset = createAction('RESET')

export const actions = {
	clock: {
		tick,
	},
	count: {
		increment,
		decrement,
		reset,
	},
}

const clock = createSlice({
	name: 'clock',
	initialState: {
		light: false,
		lastUpdate: 0,
	},
	reducers: {},
	extraReducers: builder =>
		builder.addCase(tick, (state, action) => ({
			...state,
			lastUpdate: action.payload.lastUpdate,
			light: !!action.payload.light,
		})),
})

const count = createSlice({
	name: 'count',
	initialState: 0,
	reducers: {},
	extraReducers: builder =>
		builder
			.addCase(increment, state => state + 1)
			.addCase(decrement, state => state - 1)
			.addCase(reset, () => 0),
})

const reducer = combineReducers({
	clock: clock.reducer,
	count: count.reducer,
})

export type State = ReturnType<typeof reducer>

export const store = createStore(reducer)

export const useSelector = <TSelected>(
	selector: (state: State) => TSelected,
	equalityFn?: (left: TSelected, right: TSelected) => boolean,
) => origUseSelector(selector, equalityFn)

export const useAction = <A extends unknown[]>(fn: (...a: A) => unknown) => {
	const d = useDispatch()
	return useCallback((...a: A) => d(fn(...a)), [d, fn])
}
