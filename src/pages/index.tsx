import React from 'react'
import useInterval from '~/lib/useInterval'
import Clock from '~/components/Clock'
import Counter from '~/components/Counter'
import { actions, useAction } from '~/store'

const IndexPage = () => {
	// Use state or dispatch here
	const tick = useAction(actions.clock.tick)

	// Tick the time every second
	useInterval(() => {
		tick(true)
	}, 1000)
	return (
		<>
			<Clock />
			<Counter />
		</>
	)
}

export default IndexPage
