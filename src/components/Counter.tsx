import React from 'react'
import { useSelector, actions, useAction } from '~/store'

const useCounter = () => {
	const count = useSelector(state => state.count)
	const increment = useAction(actions.count.increment)
	const decrement = useAction(actions.count.decrement)
	const reset = useAction(actions.count.reset)

	return { count, increment, decrement, reset }
}

const Counter = () => {
	const { count, increment, decrement, reset } = useCounter()
	return (
		<div>
			<h1>
				Count: <span>{count}</span>
			</h1>
			<button onClick={increment}>+1</button>
			<button onClick={decrement}>-1</button>
			<button onClick={reset}>Reset</button>
		</div>
	)
}

export default Counter
